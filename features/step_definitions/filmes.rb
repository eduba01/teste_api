#language: pt
 
    Dado("que acesso o endpoint de filmes") do
        @endereco = 'https://swapi.dev/api/films/'
        @body = {
            :content_type => 'application/json; charset=utf-8'
        }
    end

    Quando("realizar a requisição no endpoint de filmes") do
        @response = HTTParty.get(@endereco, @body)
    end
    
    Então("é retornado o código {int}") do |status_code|
        puts "\n Código de retorno da API: #{@response.code}"
        expect(@response.code).to eq(status_code) # valida se o cógigo de retorno é 200
        puts "\n"
    end
    
    Então("retorna os filmes com George Lucas como diretor e Rick McCallum como produtor") do
        @data = @response.parsed_response["results"]
  
            @data.each do | item |  # array que retorna ojetos do json
            end

            quantidade = @data.count
            i = 0
            puts "_____________ FILMES _____________ \n"

            while i < quantidade
                i.to_i
                    # Valida se o nome do diretor e do produtor do filme estão na codição
                    if @data[i]["director"].include? "George Lucas" and @data[i]["producer"].include? "Rick McCallum"
                        puts "\n - Title: " + @data[i]["title"] + "  ---  Director: " + @data[i]["director"] + ", Producer: " + @data[i]["producer"]
                    end
                i += 1
            end    

    end
     
 