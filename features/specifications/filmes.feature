#language: pt

@filmes
Funcionalidade: Busca de filmes API
    Eu como usuário do sistema
    Quero realizar requisições na API
    Para visualizar informações


Cenário: Realizar uma requisição GET com sucesso
    Dado que acesso o endpoint de filmes
    Quando realizar a requisição no endpoint de filmes
    Então é retornado o código 200

Cenário: Efetuando busca de filmes
    Dado que acesso o endpoint de filmes
    Quando realizar a requisição no endpoint de filmes
    Então retorna os filmes com George Lucas como diretor e Rick McCallum como produtor